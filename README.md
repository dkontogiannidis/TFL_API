# DESCRIPTION

This is a flask service implemented to communicate with the REST API of Tfl and acquire any disruptions that may be
occurring at a tube line. The user has the ability to schedule a task for a time he wants. Afterwards the user is able
to check at that time, any disruptions that the line or lines he is going to visit have. It can be used immediately
without having any requirements apart docker as the user is able to execute this application inside the docker
environment.

# INSTRUCTIONS

 1. Open cmd/terminal
 2. Run: docker build -t tfl_api .
 3. Run: docker run -p 5000:5000 -d tfl_api

# Communicate with the REST API

CHECK SERVICE STATUS:
    RUN: curl http://localhost:5000/status/

SCHEDULE TASK:
    RUN: curl http://localhost:5000/schedule?schedule_time=2017-10-1211:21:35&lines=bakerloo, central
    parameters:
        time    : the time for the task to run (format "%Y-%m-%d%H:%M:%S")
        lines   : the lines you will visit (if a line is not supported please add it at london_tube_lines list)
    Warning: the time must be a future time else error message is displayed
    Response: A message and the task id

GET_SCHEDULED_TASKS:
    RUN: curl http://localhost:5000/disruptions/             -> for all the tasks
    or
    RUN: curl http://localhost:5000/disruptions/?task_id=1   -> for a specific task if you know the task id
    Response : A dictionary with the requested task/tasks


# TEST THE IMPLEMENTATION

   NOTE: I could not integrate it in the docker.
   NOTE: Mock is used to use a json having disruptions as tfl had no disruptions at any line by the time of testing it.
   Requirements: Python and pip installed and included in the path

 1. Open cmd/terminal
 2. Run: pip install -r requirements.txt
 3. Run: python test_implementation.py

 # EXTRAS:

    INIT DATABASE:
        RUN: curl http://localhost:5000/init_db/
    DELETE DATABASE:
        RUN: curl http://localhost:5000/delete_db/