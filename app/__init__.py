from flask import Flask
from flask_cache import Cache
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_object('app.conf')
db = SQLAlchemy(app)

from app import controllers, models, data_handling


cache = Cache(config={'CACHE_TYPE': 'simple'})
cache.init_app(app)


