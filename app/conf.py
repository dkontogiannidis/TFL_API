from os import path

CSRF_ENABLED = True

CSRF_SESSION_KEY = "ed3c8b1d8eff3cad8bf96da8f0466a"

basedir = path.abspath(path.dirname(__file__))


SQLALCHEMY_DATABASE_URI = 'sqlite:///' + path.join(basedir, 'app.db')

time_format = "%Y-%m-%d%H:%M:%S"

# TODO: Add all the lines
london_tube_lines = [
    u'bakerloo', u'victoria', u'central', u'district', u'jubilee', u'circle', u'piccadilly'
]
