from flask import request
from app import app
from threading import Timer
from conf import time_format
from datetime import datetime
from functools import partial
from data_handling import find_disruptions, get_tasks_complete, create_new_task
from init_database_tables import create
from drop_database_tables import destroy


@app.route('/', methods=['GET', 'POST'])
@app.route('/help/')
def root():
    return "INFO PAGE"


@app.route('/schedule/', methods=['GET'])
def schedule():
    try:
        # Retrieve the GET request parameters
        schedule_time = request.args.get('schedule_time')
        lines = request.args.get('lines')
    except ValueError:
        return "WRONG PARAMETERS ERROR: The request is accepted only if it includes parameters: schedule_time, lines"
    # Format the lines received as a string to a list
    lines = lines.replace(" ", "").split(',')

    # Check if the date and time format is correct
    try:
        schedule_time = schedule_time.replace(" ", "").split(".")
        schedule_time = datetime.strptime(schedule_time[0], time_format)
        str(schedule_time.month)
    except ValueError:
        return "WRONG PARAMETERS ERROR: Wrong date format.\n Expected Format: {} \n Received: {}".format(time_format,
                                                                                                         schedule_time[0])
    # Make sure that the time provided is not in the past.
    if schedule_time >= datetime.now():
        task_id = create_new_task(schedule_time)
        t = Timer((schedule_time - datetime.now()).seconds, partial(find_disruptions, task_id, lines))
        t.start()
        return "INFO: Your request is successful. Task id = {}".format(task_id)
    else:
        return "WRONG PARAMETERS ERROR: The date and time provided must not be before the actual time."


@app.route('/disruptions/', methods=['GET'])
def show_disruptions():
    task_id = request.args.get('task_id')
    if task_id is not None:
        return str(get_tasks_complete(task_id))
    else:
        return str(get_tasks_complete())


@app.route('/status/', methods=['POST', 'GET'])
def status():
    return "Welcome! Service is running."


@app.route('/init_db/', methods=['POST', 'GET'])
def init_db():
    return create()


@app.route('/delete_db/', methods=['POST', 'GET'])
def delete_db():
    return destroy()


