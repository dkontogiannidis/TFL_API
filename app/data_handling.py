from app import db, models
from datetime import date, time
import requests
import json


def parse_tfl_to_dict(tfl_list, lines_to_visit):
    """
    Parse the list received from the TFL API to a python dictionary with the required information

    :param tfl_list: A list consisting of status data about all the london tube lines
    :param lines_to_visit: The lines which the issuer of the request will visit
    :return: {tube_line : list_of_disruptions}
    """
    try:
        tfl_list = tfl_list.json()
    except AttributeError:
        pass
    tfl_list = filter(lambda line: line["id"] in lines_to_visit or line["id"] == lines_to_visit, tfl_list)
    return {line["id"]: line["disruptions"] for line in tfl_list}


def create_new_task(schedule_time):
    """
    Create a new scheduled task to retrieve information from the Tfl API concerning disruptions in the tube lines

    :param schedule_time:
    :return:
    """
    # Start a session with the database
    session = db.session()
    # Create a new task entry in the Database
    task = models.Task()
    task.scheduled_date = date(schedule_time.year, schedule_time.month, schedule_time.day)
    task.scheduled_time = time(schedule_time.hour, schedule_time.minute, schedule_time.second).replace(microsecond=0)
    # Add the task object to the session
    session.add(task)
    # Apply changes to the database
    session.flush()
    task_id = task.id
    session.commit()
    return task_id


def find_disruptions(task_id, lines):
    """
    Communicate with through the REST API with Tfl to retrieve tube service information and then save the
    important data to the database.

    :param task_id: The id of the allocated task
    :param lines: The lines which the issuer of the request will visit
    :return: -
    """
    session = db.session()
    # Parse the tube line information
    lines = parse_tfl_to_dict(requests.get("https://api.tfl.gov.uk/line/mode/tube/status"), lines)
    # Create a new disruption object for each disruption that may affect the issuer of this request
    for tube_line, disruptions in lines.items():
        for disruption in disruptions:
            disruption_object = models.Disruption()
            disruption_object.line_id = tube_line
            disruption_object.description = disruption
            disruption_object.task_id = task_id
            session.add(disruption_object)
    session.commit()


def get_tasks_complete(task_id=None):
    """
    Convert the information contained in the database in an easy to read way for the issuer of the request

    :param task_id: None if a dictionary with all the tasks will be returned or the number off the task to be returned
    :return: dict to be provided to the request issuer as a response
    """
    tasks_dict = {}
    if task_id is None:
        tasks = models.Task.query.all()
    else:
        tasks = models.Task.query.filter_by(id=task_id).first()
        tasks_dict[str(task_id)] = {}
    try:
        for task in tasks:
            tasks_dict[str(task.id)] = {}
            tasks_dict[str(task.id)]["execution_time"] = str(task.scheduled_date), str(task.scheduled_time)
            tasks_dict[str(task.id)]["disruptions"] = {}
            for disruption in task.disruptions:
                if disruption.line_id in tasks_dict[str(task.id)]["disruptions"]:
                    tasks_dict[str(task.id)]["disruptions"][str(disruption.line_id)].append(str(disruption.description))
                else:
                    tasks_dict[str(task.id)]["disruptions"][str(disruption.line_id)] = [str(disruption.description)]
    except TypeError:
        task = tasks
        tasks_dict[str(task.id)] = {}
        tasks_dict[str(task.id)]["execution_time"] = str(task.scheduled_date), str(task.scheduled_time)
        tasks_dict[str(task.id)]["disruptions"] = {}
        for disruption in task.disruptions:
            if disruption.line_id in tasks_dict[str(task.id)]["disruptions"]:
                tasks_dict[str(task.id)]["disruptions"][str(disruption.line_id)].append(str(disruption.description))
            else:
                tasks_dict[str(task.id)]["disruptions"][str(disruption.line_id)] = [str(disruption.description)]

    return tasks_dict






