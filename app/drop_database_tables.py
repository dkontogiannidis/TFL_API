from app import db


def destroy():
    db.drop_all()
    return "OK"