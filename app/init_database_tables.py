from app import db
from conf import london_tube_lines
from models import Line

def create():
    db.create_all()

    session = db.session()
    for tube_line in london_tube_lines:
        line_object = Line()
        line_object.id = tube_line
        session.add(line_object)
    session.commit()
    session.close()
    return "OK"
