from app import db


class Task(db.Model):
    """
    Task Table

    A table containing the scheduled tasks

    :columns: id, schedule_date, schedule_time
    """
    __tablename__ = 'task'

    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True, nullable=False)
    scheduled_date = db.Column(db.DATE, nullable=False)
    scheduled_time = db.Column(db.TIME, nullable=False)

    disruptions = db.relationship('Disruption')


class Line(db.Model):
    """
    Line Table

    A table used to allow only existing London tube lines to be used

    :columns: id
    """
    __tablename__ = 'line'

    id = db.Column(db.String(100), unique=True, primary_key=True, nullable=False)


class Disruption(db.Model):
    """
    Disruption Table

    A table containing the disruptions that occurred at a specific time on a specific line.

    :columns: id, description, line_id, task_id
    """
    __tablename__ = 'disruption'

    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True, nullable=False)
    description = db.Column(db.String(255))
    line_id = db.Column(db.String(100), db.ForeignKey(Line.id))
    task_id = db.Column(db.Integer, db.ForeignKey(Task.id))





