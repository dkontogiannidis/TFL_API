import unittest
import json
from mock import patch
from datetime import datetime, timedelta
from os import path
from app.conf import basedir, time_format
from app.data_handling import find_disruptions, get_tasks_complete, create_new_task
from app import app, db
from time import sleep


class FlaskTestCase(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    @patch('app.data_handling.requests.get')
    def test_find_disruptions(self, mock_get):
        mock_get.return_value = json.load(open("D:\\Jimmy\\CODING\\TFL_API\\app\\sample_tfl_response.json", "r"))
        schedule_date = datetime.now() + timedelta(seconds=6)
        task_id = create_new_task(schedule_date)
        find_disruptions(task_id, ['bakerloo', 'central'])
        expected_dictionary = {'1': {'execution_time': (str(schedule_date.date()), str(schedule_date.time().replace(microsecond=0))),
                                     'disruptions': {'bakerloo': ['disruption1'],
                                                     'central': ['disruption1', 'disruption2']}}}
        self.assertEqual(expected_dictionary, get_tasks_complete())

    @patch('app.data_handling.requests.get')
    def test_return_only_one_task(self, mock_get):
        mock_get.return_value = json.load(open("D:\\Jimmy\\CODING\\TFL_API\\app\\sample_tfl_response.json", "r"))
        schedule_date = datetime.now() + timedelta(seconds=6)
        task_id = create_new_task(schedule_date)
        find_disruptions(task_id, ['bakerloo', 'central'])
        task_id = create_new_task(schedule_date)
        find_disruptions(task_id, ['bakerloo', 'central'])
        expected_dictionary = {'2': {'execution_time': (str(schedule_date.date()), str(schedule_date.time().replace(microsecond=0))),
                                     'disruptions': {'bakerloo': ['disruption1'],
                                                     'central': ['disruption1', 'disruption2']}}}
        self.assertEqual(expected_dictionary, get_tasks_complete(task_id=2))

    def test_successful_request(self):
        schedule_date = datetime.now() + timedelta(seconds=6)
        schedule_date.strftime(time_format)
        schedule_date.replace(microsecond=0)
        response = self.app.get("/schedule/?schedule_time={}&lines=central, bakerloo".format(str(schedule_date)))
        sleep(6)
        self.assertEqual("INFO: Your request is successful. Task id = 1", response.get_data())

    def test_two_timers(self):
        schedule_date = datetime.now() + timedelta(seconds=6)
        schedule_date.strftime(time_format)
        schedule_date.replace(microsecond=0)
        self.app.get("/schedule/?schedule_time={}&lines=central, bakerloo".format(str(schedule_date)))
        sleep(1)
        schedule_date = datetime.now() + timedelta(seconds=6)
        schedule_date.strftime(time_format)
        schedule_date.replace(microsecond=0)
        self.app.get("/schedule/?schedule_time={}&lines=central, bakerloo".format(str(schedule_date)))
        sleep(6)
        self.assertItemsEqual(['1', '2'], get_tasks_complete().keys())


if __name__ == '__main__':
    unittest.main()
